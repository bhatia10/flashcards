# flashcards for social media management class

# welcome message
print("""
  These flashcards are for the class PR 330: Social Media Management.
    Things you should know:
    
    1. You must answer each question correctly before being able to move on to the next one. 
    2. You have unlimited tries on each question. 
    3. Make sure there are no spelling errors in your typed answers.
    4. You can skip a question by entering 'skip' at any time. 
    5. You can quit the flashcards program by entering 'quit' at any time. 
  
    Best of luck and happy studying!
  """)

# question 1
count = 1
while True:
  print("""
  Growth Hacking + Content Marketing = 
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "content hacking"):
    print("You are right!")
    print("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 2
count = 1

while True:
  print("""
        What is the last step in content hacking?""")
  answer = input("Type in your answer: ").lower()

  if (answer == "analyze results") or (answer == "analyze the results") or (answer == "result analysis") or (answer == "analyze result") :
    print("You are right!")
    print("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 3
count = 1

while True:
  print("""
        You should always focus on: One key goal+ one key metric + ?""")
  answer = input("Type in your answer: ").lower()

  if (answer == "specific timeline"):
    print("You are right!")
    print("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 4
count = 1

while True:
  print("""
        What type of content is created from a unique angle that solves a problem for your audience without repeating what's already being done by your competitors?""")
  answer = input("Type in your answer: ").lower()

  if (answer == "competition-free content") or (answer == "competition free content") :
    print("You are right!")
    print("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 5
count = 1

while True:
  print("""
  What is the intersection between content that your customers care about and what you have to offer to them called?
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "content core"):
    print("You are right!")
    print("You answered this question in", count, "tries.")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 6
count = 1

while True:
  print("""
  What is this the definition of: Content that is somewhat related to your industry, but does not directly intersect with what you have to offer
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "parallel content"):
    print("You are right!")
    print ("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 7
count = 1

while True:
  print("""
  What are these examples of: Free digital tools/quizzes, free online courses, eBooks, guides?
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "big content ideas"):
    print("You are right!")
    print ("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 8
count = 1

while True:
  print("""
  What type of research already exists and you can tap into?
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "secondary research"):
    print("You are right!")
    print("you answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 9
count = 1

while True:
  print("""
  What are words relevant to your brand, competitors, and industry that help with SEO called? 
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "keywords"):
    print("You are right!")
    print("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

# question 10
count = 1

while True:
  print("""
  Which of the following is NOT an example of free secondary research: government reports, company-published research, statista, wearesocial.com?
  """)
  answer = input("Type in your answer: ").lower()

  if (answer == "statista"):
    print("You are right!")
    print ("You answered this question in", count, "tries.")
    break
  elif (answer == "quit"):
    print("See you next time!")
    exit()
  elif (answer == "skip"):
    print("Ok let's go to next problem!")
    break
  else:
    print("Wrong! Try one more time!")
    count = count + 1

print("""
Congratulations on completing all the questions!
See you next time!
""")
